# Gitlab Installation 

Install gitlab in droplet

##  login to ssh

- check you are on root or not

```
sudo su

Enter your password of sytem

```

## ssh login for droplet

ssh root@159.203.119.168

```
Enter Password:

```

## Before Installing Need to refersh local packages before Install any  dependencies
```
sudo apt-get update

```

## First install postfix in server to send email

```
sudo apt-get install ca-certificates curl openssh-server postfix

```
## After Installing postfix
```
    - You have red color screen popup will show to select the options
    - select Internet Site when prompted
    -Add IP address(159.203.119.168) to configure how the system will send mail 
```

## Install gitlab 
##  Move to temp directory

```
cd /tmp    
curl -LO https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh

```
## Examine the sccript

```
less /tmp/script.deb.sh

```
## Run the installer to manage the GitLab repositories

```
sudo bash /tmp/script.deb.sh

```
## Now Installing gitlab application it will install necessary components on your server.

```
sudo apt-get install gitlab-ce

```
##Adjusting the Firewall Rules
## check status of firewall

```
sudo ufw status

```
## Status is not active then enable it on your server it is off you need to enable it is already install in our server

```
sudo ufw enable
sudo ufw status

```
## Enable some firewal extensions

```
sudo ufw allow http
sudo ufw allow https
sudo ufw allow OpenSSH
```
## Infomation for another server if ufw not install you need to type
```
sudo apt-get install ufw
sudo ufw status
sudo ufw allow http
sudo ufw allow https
sudo ufw allow OpenSSH
```
## Editing the GitLab Configuration File
```
sudo nano /etc/gitlab/gitlab.rb
#( Note:- To edit Nano file Please Press Ctrl+O then Press Ctrl+M after this Press Ctrl+X )

```
## If your GitLab server does not have a domain name, you will need to use an IP its with http
```
external_url 'http://159.203.119.168'
```
## If you want to setup with https follow this command

```
sudo nano /etc/gitlab/gitlab.rb
external_url 'http://159.203.119.168'
letsencrypt['enable'] = true
letsencrypt['contact_emails'] = ['Info@dap-udea.co']
```

## Run the command to reconfigure Gitlab

```
    sudo gitlab-ctl reconfigure
```

## gitlab is installed now you can acess with 
```
http://159.203.119.168
```
## with lets encrypt you can acess with
```
https://159.203.119.168
```
##  After Redirected to the conventional GitLab login page
## Change password and username is root

## Adding an SSH Key to your Account
```
ssh-keygen -o -t rsa -C "Info@dap-udea.co" -b 4096
cat ~/.ssh/id_rsa.pub
```
## ssh key is generated 
```
The key fingerprint is:
SHA256:OW1AOodmTFZb9cHU5aaMMRcWanLEpZVtMBaXaAuUkrY Info@dap-udea.co
The key's randomart image is:
+---[RSA 4096]----+
|      o.o ++++#B=|
|     + + * +.XoB+|
|      B = + X +.o|
|     o o E + B o |
|        S o . o  |
|         o       |
|                 |
|                 |
|                 |
+----[SHA256]-----+
```
## To display your public key
```
cat ~/.ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC/EQ5LiXtZHLr6uaDKg39Z0XloImpCHSljYYR69snPZmHilNc6jRDDSvUnClbqCUnqYbvrDQIO2a3USg46KW8GgGqnJlQl7EtBOaSHlLl/NSFflHhCnTgh56pWf0ujEmWhoiL+cFDv5+PNleDM0gnptqo5SJo1KZ0kgbBDxMoEVF5D2+F4aZxuUk5KpVEgIj3Fx7/HyCLPnVF6tds//wAyBLwJF1amTx164WvyvzLvV07pNgI/cE4hjr+0BisN9LrGBqjrDNqPaqWHsnJc+VE3IJqIU2d0okxKmnzQJGbu6xysMXsHAELLnOYN1F4uWWwrPU+MfJiw/42HqSy9rC5C0JBm/yLxL9Ou+Ay7uQtsorStyx16c96BVVG+B2j3tIXSlue1IEkWg4XPtQCi2n2qSLl7gHz631DG9ULyPpEqsj1WYPRGMmzl7jSGPLVXphv/yUsN7bCdw4whNRHl8EIDw07S/RcynrLKA+eK+gPjgZ8hkg9mQP5xOEwGavzvnBvaoOWL2x7TXahv32+9RO8KBvL/imtUgnZbLtXhjXJhqrEk/AQZYnhyNz1lMUUULro/k3vB0rOnyap0zDDpE3i0mL21nwwh8zpO/kW6d79cCnCnLi1rLLwMhpL22YKPIj5YT1rWWJiSQHKQ66KhIXR9s2DNmNu0o8Gw9fOYyZST+Q== Info@dap-udea.co
```

## Copy the block of text that's displayed and head back to your Profile Settings in GitLab's web interface go to add sshkeys in left side interface
    
## cron jobs To Automatically Renew Let's Encrypt Certificates
```
sudo nano /etc/cron.daily/gitlab-le

```
## paste this script in open file
```
#!/bin/bash

set -e

/usr/bin/gitlab-ctl renew-le-certs > /dev/null
#( Note:- To edit Nano file Please Press Ctrl+O then Press Ctrl+M after this Press Ctrl+X )
```

## Excute the edited file with the command
```
sudo chmod +x /etc/cron.daily/gitlab-le

```

## basic commands for pull and push
```
git clone git@159.203.119.168:root/cheatsheet-for-ubuntu-16.04.git
git status
git add .  # to add all files
git add 'filename'  #to add one particular file
git commit -m "commit message"
git push origin master
git pull origin master  ## master is branch if you are working in developer branch then you write developer if multiple developer is working on project please take a pull before cmmit
git branch  ## to check the branch name

```


## Gitlab check automatically check each day if its Let's Encrypt certificate needs to be renewed.

